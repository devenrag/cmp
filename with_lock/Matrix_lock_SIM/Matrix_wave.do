onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix binary /cmp_tb/uut_cmp/Test_mode
add wave -noupdate -radix unsigned /cmp_tb/Clknumber
add wave -noupdate /cmp_tb/Clk
add wave -noupdate -radix binary /cmp_tb/uut_cmp/Resetb
add wave -noupdate -divider Bus_Arbiter
add wave -noupdate -radix binary /cmp_tb/uut_cmp/bus_arbiter/Resetb
add wave -noupdate -radix binary /cmp_tb/uut_cmp/bus_arbiter/BREQ0B
add wave -noupdate -radix binary /cmp_tb/uut_cmp/bus_arbiter/BREQ1B
add wave -noupdate -radix binary /cmp_tb/uut_cmp/bus_arbiter/BREQ2B
add wave -noupdate -radix binary /cmp_tb/uut_cmp/bus_arbiter/BREQ3B
add wave -noupdate -radix binary /cmp_tb/uut_cmp/bus_arbiter/BUSYB_B_IN
add wave -noupdate /cmp_tb/uut_cmp/bus_arbiter/BusGrant0B
add wave -noupdate /cmp_tb/uut_cmp/bus_arbiter/BusGrant1B
add wave -noupdate /cmp_tb/uut_cmp/bus_arbiter/BusGrant2B
add wave -noupdate /cmp_tb/uut_cmp/bus_arbiter/BusGrant3B
add wave -noupdate -divider CMP_TOP
add wave -noupdate -radix binary /cmp_tb/uut_cmp/BUSRD_B_to_core
add wave -noupdate -radix binary /cmp_tb/uut_cmp/BUSRDX_B_to_core
add wave -noupdate -radix binary /cmp_tb/uut_cmp/BUSUPGR_B_to_core
add wave -noupdate -radix binary /cmp_tb/uut_cmp/HelpB_to_core
add wave -noupdate -radix binary /cmp_tb/uut_cmp/ShareB_to_core
add wave -noupdate -radix binary /cmp_tb/uut_cmp/BUSYB_to_arbiter
add wave -noupdate -divider MM_Signals
add wave -noupdate -radix binary /cmp_tb/uut_cmp/uut_MM/renb
add wave -noupdate -radix binary /cmp_tb/uut_cmp/uut_MM/wenb
add wave -noupdate -radix hexadecimal /cmp_tb/uut_cmp/uut_MM/data_in
add wave -noupdate -radix hexadecimal /cmp_tb/uut_cmp/uut_MM/addr_in
add wave -noupdate -radix hexadecimal /cmp_tb/uut_cmp/uut_MM/data_out
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {4200827 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 100
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {2797738 ps} {5510042 ps}
