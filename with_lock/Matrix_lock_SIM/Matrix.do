# File name: Matrix.do
vsim +nowarnTSCALE -do nowarnings_arith_numeric.do -lib work -c -t 1ps -novopt cmp_tb 
log * -r
do Matrix_wave.do
run 100ns
mem save -o DM_Initial_Content.mem -f mti -data hex -addr hex -startaddress 0 -endaddress 35 -wordsperline 4 /cmp_tb/uut_cmp/uut_MM/Data_MM/d_mem
run 30us
mem save -o DM.mem -f mti -data hex -addr hex -startaddress 0 -endaddress 35 -wordsperline 4 /cmp_tb/uut_cmp/uut_MM/Data_MM/d_mem
echo "Check the last line of the DM.mem produced."
echo "If it is  20: 00000000 000004d8 00000010 00000000" 
echo "then congratulations!"
