library verilog;
use verilog.vl_types.all;
entity read_write_arbiter is
    port(
        Clk             : in     vl_logic;
        Resetb          : in     vl_logic;
        CAG             : in     vl_logic;
        LW              : in     vl_logic;
        SW              : in     vl_logic_vector(0 to 3);
        CCU_LW_Grant    : out    vl_logic;
        CCU_SW_Grant    : out    vl_logic_vector(0 to 3)
    );
end read_write_arbiter;
