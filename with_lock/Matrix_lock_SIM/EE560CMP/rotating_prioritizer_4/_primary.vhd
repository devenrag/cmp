library verilog;
use verilog.vl_types.all;
entity rotating_prioritizer_4 is
    port(
        Clk             : in     vl_logic;
        Resetb          : in     vl_logic;
        GT_Utilized     : in     vl_logic;
        RQ              : in     vl_logic_vector(0 to 3);
        GT              : out    vl_logic_vector(0 to 3)
    );
end rotating_prioritizer_4;
