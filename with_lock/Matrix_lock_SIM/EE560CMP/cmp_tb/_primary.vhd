library verilog;
use verilog.vl_types.all;
entity cmp_tb is
    generic(
        Clk_period      : integer := 10
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of Clk_period : constant is 1;
end cmp_tb;
