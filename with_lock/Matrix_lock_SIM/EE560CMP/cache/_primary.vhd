library verilog;
use verilog.vl_types.all;
entity cache is
    generic(
        DATA_WIDTH      : integer := 32;
        ADDR_WIDTH      : integer := 32;
        TAG_WIDTH       : integer := 24;
        SET_WIDTH       : integer := 4;
        STATE_WIDTH     : integer := 4;
        SET_PLUSE_WORD_WIDTH: integer := 6;
        MODIFIED        : vl_logic_vector(0 to 3) := (Hi1, Hi1, Hi1, Hi0);
        OWNER           : vl_logic_vector(0 to 3) := (Hi1, Hi1, Hi0, Hi0);
        EXCLUSIVE       : vl_logic_vector(0 to 3) := (Hi1, Hi0, Hi1, Hi0);
        SHAREHOLDER     : vl_logic_vector(0 to 3) := (Hi1, Hi0, Hi0, Hi0);
        SHAREOWNER      : vl_logic_vector(0 to 3) := (Hi1, Hi0, Hi0, Hi1);
        INVALID         : vl_logic_vector(0 to 3) := (Hi0, Hi0, Hi1, Hi0);
        core_number     : integer := 5
    );
    port(
        clk             : in     vl_logic;
        reset_b         : in     vl_logic;
        CCU_addr_in     : in     vl_logic_vector(31 downto 0);
        CCU_data_in     : in     vl_logic_vector(31 downto 0);
        CCU_rd_in       : in     vl_logic;
        CCU_wr_in       : in     vl_logic;
        SCU_data_in     : in     vl_logic_vector(31 downto 0);
        SCU_addr_in     : in     vl_logic_vector(31 downto 0);
        SCU_state_in    : in     vl_logic_vector(3 downto 0);
        SCU_data_wr_in  : in     vl_logic;
        SCU_tag_wr_in   : in     vl_logic;
        SCU_state_updateB: in     vl_logic;
        CCU_data_out    : out    vl_logic_vector(31 downto 0);
        CCU_state_out   : out    vl_logic_vector(3 downto 0);
        SCU_tag_out     : out    vl_logic_vector;
        CCU_hit         : out    vl_logic;
        SCU_MMWR_out    : out    vl_logic;
        SCU_data_out    : out    vl_logic_vector(31 downto 0);
        SCU_state_out   : out    vl_logic_vector(3 downto 0);
        SCU_hit         : out    vl_logic;
        CCU_word_reg_addr_in: in     vl_logic_vector;
        CCU_set_in      : in     vl_logic_vector;
        CCU_tag_in      : in     vl_logic_vector;
        CCU_write       : in     vl_logic;
        BusGrantB       : in     vl_logic;
        BUSRD_B_INOUT   : in     vl_logic;
        BUSRDX_B_INOUT  : in     vl_logic;
        BUSUPGR_B_INOUT : in     vl_logic;
        HelpB_INOUT     : in     vl_logic;
        tag_scu_res_ren : in     vl_logic;
        data_scu_res_ren: in     vl_logic;
        tag_scu_req_ren : in     vl_logic;
        data_scu_req_ren: in     vl_logic;
        CCU_ThreadID    : in     vl_logic_vector(1 downto 0);
        Final_SHR_Grant : in     vl_logic_vector(0 to 11);
        CCU_addr_reg    : in     vl_logic_vector(31 downto 0);
        Cache_INVALID_LL_TAG: out    vl_logic_vector(31 downto 0)
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of DATA_WIDTH : constant is 1;
    attribute mti_svvh_generic_type of ADDR_WIDTH : constant is 1;
    attribute mti_svvh_generic_type of TAG_WIDTH : constant is 1;
    attribute mti_svvh_generic_type of SET_WIDTH : constant is 1;
    attribute mti_svvh_generic_type of STATE_WIDTH : constant is 1;
    attribute mti_svvh_generic_type of SET_PLUSE_WORD_WIDTH : constant is 1;
    attribute mti_svvh_generic_type of MODIFIED : constant is 1;
    attribute mti_svvh_generic_type of OWNER : constant is 1;
    attribute mti_svvh_generic_type of EXCLUSIVE : constant is 1;
    attribute mti_svvh_generic_type of SHAREHOLDER : constant is 1;
    attribute mti_svvh_generic_type of SHAREOWNER : constant is 1;
    attribute mti_svvh_generic_type of INVALID : constant is 1;
    attribute mti_svvh_generic_type of core_number : constant is 1;
end cache;
