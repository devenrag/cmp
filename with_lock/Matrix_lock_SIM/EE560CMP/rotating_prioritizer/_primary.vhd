library verilog;
use verilog.vl_types.all;
entity rotating_prioritizer is
    port(
        Clk             : in     vl_logic;
        Resetb          : in     vl_logic;
        SUGb            : in     vl_logic;
        RQ              : in     vl_logic_vector(0 to 3);
        WQ              : in     vl_logic_vector(0 to 3);
        FQ              : in     vl_logic_vector(0 to 3);
        Final_SHR_Grant : out    vl_logic_vector(0 to 11)
    );
end rotating_prioritizer;
