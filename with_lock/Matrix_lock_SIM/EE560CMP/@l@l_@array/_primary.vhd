library verilog;
use verilog.vl_types.all;
entity LL_Array is
    generic(
        DATA_WIDTH      : integer := 32;
        ADDR_WIDTH      : integer := 7
    );
    port(
        Clk             : in     vl_logic;
        Resetb          : in     vl_logic;
        MEM_ThreadID    : in     vl_logic_vector(1 downto 0);
        MEM_LL_Hit      : in     vl_logic;
        MEM_LL_Address  : in     vl_logic_vector;
        LL_RMSHR_LL_Complete0: in     vl_logic;
        RMSHR_Address0  : in     vl_logic_vector;
        LL_RMSHR_LL_Complete1: in     vl_logic;
        RMSHR_Address1  : in     vl_logic_vector;
        LL_RMSHR_LL_Complete2: in     vl_logic;
        RMSHR_Address2  : in     vl_logic_vector;
        LL_RMSHR_LL_Complete3: in     vl_logic;
        RMSHR_Address3  : in     vl_logic_vector;
        SB_SC_Complete0 : in     vl_logic;
        SB_Address0     : in     vl_logic_vector;
        SB_SC_Complete1 : in     vl_logic;
        SB_Address1     : in     vl_logic_vector;
        SB_SC_Complete2 : in     vl_logic;
        SB_Address2     : in     vl_logic_vector;
        SB_SC_Complete3 : in     vl_logic;
        SB_Address3     : in     vl_logic_vector;
        LL_Invalidate_Request: in     vl_logic;
        Invalidate_Address: in     vl_logic_vector;
        LL_Bit_output0  : out    vl_logic;
        LL_Bit_output1  : out    vl_logic;
        LL_Bit_output2  : out    vl_logic;
        LL_Bit_output3  : out    vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of DATA_WIDTH : constant is 1;
    attribute mti_svvh_generic_type of ADDR_WIDTH : constant is 1;
end LL_Array;
