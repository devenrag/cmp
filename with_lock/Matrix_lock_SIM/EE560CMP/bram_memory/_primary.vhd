library verilog;
use verilog.vl_types.all;
entity bram_memory is
    generic(
        ADDR_WIDTH      : integer := 5;
        DATA_WIDTH      : integer := 32
    );
    port(
        clka            : in     vl_logic;
        wea             : in     vl_logic;
        web             : in     vl_logic;
        addra           : in     vl_logic_vector;
        addrb           : in     vl_logic_vector;
        dia             : in     vl_logic_vector;
        dib             : in     vl_logic_vector;
        doa             : out    vl_logic_vector;
        dob             : out    vl_logic_vector
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of ADDR_WIDTH : constant is 1;
    attribute mti_svvh_generic_type of DATA_WIDTH : constant is 1;
end bram_memory;
