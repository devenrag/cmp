----------------- Register Writes List ----------------------
|        Clock#     | Register Written  |   Data Written    |
-------------------------------------------------------------
|  CLK_CNT =     69  |         $1        |      00000009     |
-------------------------------------------------------------
|  CLK_CNT =    181  |         $2        |      00000009     |
-------------------------------------------------------------
|  CLK_CNT =    185  |         $3        |      00000051     |
-------------------------------------------------------------
|  CLK_CNT =    273  |         $4        |      00000000     |
-------------------------------------------------------------
|  CLK_CNT =    276  |         $4        |      00000001     |
-------------------------------------------------------------
|  CLK_CNT =    326  |         $4        |      00000000     |
-------------------------------------------------------------
|  CLK_CNT =    364  |         $4        |      00000000     |
-------------------------------------------------------------
|  CLK_CNT =    367  |         $4        |      00000001     |
-------------------------------------------------------------
|  CLK_CNT =    432  |         $4        |      00000000     |
-------------------------------------------------------------
|  CLK_CNT =    484  |         $4        |      00000001     |
-------------------------------------------------------------
|  CLK_CNT =    495  |         $4        |      00000001     |
-------------------------------------------------------------
|  CLK_CNT =    563  |         $4        |      00000000     |
-------------------------------------------------------------
|  CLK_CNT =    566  |         $4        |      00000001     |
-------------------------------------------------------------
|  CLK_CNT =    656  |         $4        |      00000000     |
-------------------------------------------------------------
|  CLK_CNT =    668  |         $4        |      00000000     |
-------------------------------------------------------------
|  CLK_CNT =    675  |         $4        |      00000001     |
-------------------------------------------------------------
|  CLK_CNT =    678  |         $4        |      00000000     |
-------------------------------------------------------------
|  CLK_CNT =    695  |         $4        |      00000001     |
-------------------------------------------------------------
|  CLK_CNT =    729  |         $4        |      00000000     |
-------------------------------------------------------------
|  CLK_CNT =    732  |         $4        |      00000001     |
-------------------------------------------------------------
|  CLK_CNT =    764  |         $4        |      00000000     |
-------------------------------------------------------------
|  CLK_CNT =    790  |         $4        |      00000000     |
-------------------------------------------------------------
|  CLK_CNT =    793  |         $4        |      00000001     |
-------------------------------------------------------------
|  CLK_CNT =    846  |         $4        |      00000000     |
-------------------------------------------------------------
|  CLK_CNT =    858  |         $4        |      00000000     |
-------------------------------------------------------------
|  CLK_CNT =    866  |         $4        |      00000001     |
-------------------------------------------------------------
|  CLK_CNT =    869  |         $4        |      00000000     |
-------------------------------------------------------------
|  CLK_CNT =    922  |         $4        |      00000000     |
-------------------------------------------------------------
|  CLK_CNT =    925  |         $4        |      00000001     |
-------------------------------------------------------------
|  CLK_CNT =    951  |         $4        |      00000000     |
-------------------------------------------------------------
|  CLK_CNT =    974  |         $4        |      00000000     |
-------------------------------------------------------------
|  CLK_CNT =    977  |         $4        |      00000001     |
-------------------------------------------------------------
|  CLK_CNT =   1000  |         $4        |      00000000     |
-------------------------------------------------------------
|  CLK_CNT =   1024  |         $4        |      00000000     |
-------------------------------------------------------------
|  CLK_CNT =   1027  |         $4        |      00000001     |
-------------------------------------------------------------
|  CLK_CNT =   1056  |         $4        |      00000000     |
-------------------------------------------------------------
|  CLK_CNT =   1081  |         $4        |      00000000     |
-------------------------------------------------------------
|  CLK_CNT =   1084  |         $4        |      00000001     |
-------------------------------------------------------------
|  CLK_CNT =   1108  |         $4        |      00000000     |
-------------------------------------------------------------
|  CLK_CNT =   1132  |         $4        |      00000000     |
-------------------------------------------------------------
|  CLK_CNT =   1135  |         $4        |      00000001     |
-------------------------------------------------------------
|  CLK_CNT =   1152  |         $4        |      00000000     |
-------------------------------------------------------------
|  CLK_CNT =   1165  |         $4        |      00000001     |
-------------------------------------------------------------
|  CLK_CNT =   1180  |         $4        |      00000001     |
-------------------------------------------------------------
|  CLK_CNT =   1197  |         $4        |      00000001     |
-------------------------------------------------------------
|  CLK_CNT =   1214  |         $4        |      00000000     |
-------------------------------------------------------------
|  CLK_CNT =   1222  |         $4        |      00000001     |
-------------------------------------------------------------
|  CLK_CNT =   1227  |         $4        |      00000001     |
-------------------------------------------------------------
|  CLK_CNT =   1241  |         $5        |      0000040E     |
-------------------------------------------------------------
|  CLK_CNT =   1244  |         $5        |      0000045F     |
-------------------------------------------------------------
|  CLK_CNT =   1249  |         $6        |      0000000E     |
-------------------------------------------------------------
|  CLK_CNT =   1251  |         $6        |      0000000F     |
-------------------------------------------------------------
|  CLK_CNT =   1256  |         $7        |      00000010     |
-------------------------------------------------------------
|  CLK_CNT =   1257  |         $6        |      0000000F     |
