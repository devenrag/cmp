library verilog;
use verilog.vl_types.all;
entity bram_memory_Data_MM is
    generic(
        ADDR_WIDTH      : integer := 10;
        DATA_WIDTH      : integer := 32;
        ADDR            : integer := 10
    );
    port(
        clk             : in     vl_logic;
        wea             : in     vl_logic;
        rea             : in     vl_logic;
        addra           : in     vl_logic_vector;
        dia             : in     vl_logic_vector;
        doa             : out    vl_logic_vector;
        web             : in     vl_logic;
        reb             : in     vl_logic;
        addrb           : in     vl_logic_vector;
        dib             : in     vl_logic_vector;
        dob             : out    vl_logic_vector
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of ADDR_WIDTH : constant is 1;
    attribute mti_svvh_generic_type of DATA_WIDTH : constant is 1;
    attribute mti_svvh_generic_type of ADDR : constant is 1;
end bram_memory_Data_MM;
