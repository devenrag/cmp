library verilog;
use verilog.vl_types.all;
entity MM is
    generic(
        DATA_WIDTH      : integer := 32;
        ADDR_WIDTH      : integer := 10;
        MM_state_type_INI: integer := 0;
        MM_state_type_Read_count: integer := 1;
        MM_state_type_Read_out: integer := 2;
        MM_state_type_Write_count: integer := 3;
        MM_state_type_Write_in: integer := 4
    );
    port(
        Clk             : in     vl_logic;
        Resetb          : in     vl_logic;
        renb            : in     vl_logic;
        wenb            : in     vl_logic;
        data_in         : in     vl_logic_vector;
        addr_in         : in     vl_logic_vector(31 downto 0);
        data_out        : out    vl_logic_vector;
        busyb           : out    vl_logic;
        BusGrant0B      : in     vl_logic;
        BusGrant1B      : in     vl_logic;
        BusGrant2B      : in     vl_logic;
        BusGrant3B      : in     vl_logic;
        fio_mmem_addr   : in     vl_logic_vector(31 downto 0);
        fio_mmem_data_out: out    vl_logic_vector;
        fio_mmem_data_in: in     vl_logic_vector;
        fio_mmem_wea    : in     vl_logic;
        fio_mmem_rea    : in     vl_logic;
        fio_mmem_test_mode: in     vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of DATA_WIDTH : constant is 1;
    attribute mti_svvh_generic_type of ADDR_WIDTH : constant is 1;
    attribute mti_svvh_generic_type of MM_state_type_INI : constant is 1;
    attribute mti_svvh_generic_type of MM_state_type_Read_count : constant is 1;
    attribute mti_svvh_generic_type of MM_state_type_Read_out : constant is 1;
    attribute mti_svvh_generic_type of MM_state_type_Write_count : constant is 1;
    attribute mti_svvh_generic_type of MM_state_type_Write_in : constant is 1;
end MM;
