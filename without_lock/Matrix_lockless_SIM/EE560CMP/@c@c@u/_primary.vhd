library verilog;
use verilog.vl_types.all;
entity CCU is
    generic(
        DATA_WIDTH      : integer := 32;
        ADDR_WIDTH      : integer := 32;
        TAG_WIDTH       : integer := 24;
        SET_WIDTH       : integer := 4;
        CACHE_DEEP      : integer := 5;
        STATE_WIDTH     : integer := 4;
        MODIFIED        : vl_logic_vector(0 to 3) := (Hi1, Hi1, Hi1, Hi0);
        OWNER           : vl_logic_vector(0 to 3) := (Hi1, Hi1, Hi0, Hi0);
        EXCLUSIVE       : vl_logic_vector(0 to 3) := (Hi1, Hi0, Hi1, Hi0);
        SHAREHOLDER     : vl_logic_vector(0 to 3) := (Hi1, Hi0, Hi0, Hi0);
        SHAREOWNER      : vl_logic_vector(0 to 3) := (Hi1, Hi0, Hi0, Hi1);
        INVALID         : vl_logic_vector(0 to 3) := (Hi0, Hi0, Hi1, Hi0)
    );
    port(
        clk             : in     vl_logic;
        reset_b         : in     vl_logic;
        CCU_rd_in       : in     vl_logic;
        CCU_wr_in       : in     vl_logic;
        CCU_addr_in     : in     vl_logic_vector(31 downto 0);
        Cache_state_in  : in     vl_logic_vector(3 downto 0);
        Cache_hit_in    : in     vl_logic;
        Cache_wr_out    : out    vl_logic;
        Cache_rd_out    : out    vl_logic;
        Cache_hit_out   : out    vl_logic;
        Cache_tag_out   : out    vl_logic_vector(23 downto 0);
        Cache_word_reg_addr_out: out    vl_logic_vector(5 downto 0);
        Cache_set_out   : out    vl_logic_vector(3 downto 0);
        CACHE_INST_BIT  : in     vl_logic;
        LL_bit          : in     vl_logic;
        SC_valid        : in     vl_logic;
        CCU_SC_Complete : out    vl_logic;
        SC_successful   : out    vl_logic;
        CCU_write       : out    vl_logic;
        CCU_ThreadID    : in     vl_logic_vector(1 downto 0);
        Cache_ThreadID  : out    vl_logic_vector(1 downto 0);
        CCU_addr_reg    : out    vl_logic_vector(31 downto 0)
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of DATA_WIDTH : constant is 1;
    attribute mti_svvh_generic_type of ADDR_WIDTH : constant is 1;
    attribute mti_svvh_generic_type of TAG_WIDTH : constant is 1;
    attribute mti_svvh_generic_type of SET_WIDTH : constant is 1;
    attribute mti_svvh_generic_type of CACHE_DEEP : constant is 1;
    attribute mti_svvh_generic_type of STATE_WIDTH : constant is 1;
    attribute mti_svvh_generic_type of MODIFIED : constant is 1;
    attribute mti_svvh_generic_type of OWNER : constant is 1;
    attribute mti_svvh_generic_type of EXCLUSIVE : constant is 1;
    attribute mti_svvh_generic_type of SHAREHOLDER : constant is 1;
    attribute mti_svvh_generic_type of SHAREOWNER : constant is 1;
    attribute mti_svvh_generic_type of INVALID : constant is 1;
end CCU;
