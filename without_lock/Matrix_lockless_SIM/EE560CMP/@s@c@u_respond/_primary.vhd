library verilog;
use verilog.vl_types.all;
entity SCU_respond is
    generic(
        DATA_WIDTH      : integer := 32;
        ADDR_WIDTH      : integer := 32;
        TAG_WIDTH       : integer := 24;
        SET_WIDTH       : integer := 4;
        STATE_WIDTH     : integer := 4;
        MODIFIED        : vl_logic_vector(0 to 3) := (Hi1, Hi1, Hi1, Hi0);
        OWNER           : vl_logic_vector(0 to 3) := (Hi1, Hi1, Hi0, Hi0);
        EXCLUSIVE       : vl_logic_vector(0 to 3) := (Hi1, Hi0, Hi1, Hi0);
        SHAREHOLDER     : vl_logic_vector(0 to 3) := (Hi1, Hi0, Hi0, Hi0);
        SHAREOWNER      : vl_logic_vector(0 to 3) := (Hi1, Hi0, Hi0, Hi1);
        INVALID         : vl_logic_vector(0 to 3) := (Hi0, Hi0, Hi1, Hi0);
        state_respond_initial_res: integer := 0;
        state_respond_tag_check: integer := 2;
        state_respond_busrd_busrdx_res: integer := 1;
        state_respond_dummy: integer := 3
    );
    port(
        CLK             : in     vl_logic;
        RESETB          : in     vl_logic;
        HIT_C_IN        : in     vl_logic;
        MOESISTATE_C_IN : in     vl_logic_vector(3 downto 0);
        DATA_C_IN       : in     vl_logic_vector(31 downto 0);
        ADDR_B_IN       : in     vl_logic_vector(31 downto 0);
        MOESIEN_C_OUT   : out    vl_logic;
        ADDR_C_OUT      : inout  vl_logic_vector(31 downto 0);
        MOESISTATE_C_OUT: inout  vl_logic_vector(3 downto 0);
        SCU_INVALIDATE  : out    vl_logic;
        DATA_B_INOUT    : inout  vl_logic_vector(31 downto 0);
        HelpB_B_OUT     : out    vl_logic;
        BUSRD_B_IN      : in     vl_logic;
        BUSRDX_B_IN     : in     vl_logic;
        BUSUPGR_B_IN    : in     vl_logic;
        BusGrantB       : in     vl_logic;
        tag_scu_res_ren : out    vl_logic;
        data_scu_res_ren: out    vl_logic;
        reshelp         : out    vl_logic;
        ShareB_B_OUT    : out    vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of DATA_WIDTH : constant is 1;
    attribute mti_svvh_generic_type of ADDR_WIDTH : constant is 1;
    attribute mti_svvh_generic_type of TAG_WIDTH : constant is 1;
    attribute mti_svvh_generic_type of SET_WIDTH : constant is 1;
    attribute mti_svvh_generic_type of STATE_WIDTH : constant is 1;
    attribute mti_svvh_generic_type of MODIFIED : constant is 1;
    attribute mti_svvh_generic_type of OWNER : constant is 1;
    attribute mti_svvh_generic_type of EXCLUSIVE : constant is 1;
    attribute mti_svvh_generic_type of SHAREHOLDER : constant is 1;
    attribute mti_svvh_generic_type of SHAREOWNER : constant is 1;
    attribute mti_svvh_generic_type of INVALID : constant is 1;
    attribute mti_svvh_generic_type of state_respond_initial_res : constant is 1;
    attribute mti_svvh_generic_type of state_respond_tag_check : constant is 1;
    attribute mti_svvh_generic_type of state_respond_busrd_busrdx_res : constant is 1;
    attribute mti_svvh_generic_type of state_respond_dummy : constant is 1;
end SCU_respond;
