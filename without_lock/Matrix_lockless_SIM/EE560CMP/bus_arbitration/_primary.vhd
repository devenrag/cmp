library verilog;
use verilog.vl_types.all;
entity bus_arbitration is
    port(
        Clk             : in     vl_logic;
        Resetb          : in     vl_logic;
        BREQ0B          : in     vl_logic;
        BREQ1B          : in     vl_logic;
        BREQ2B          : in     vl_logic;
        BREQ3B          : in     vl_logic;
        BusGrant0B      : out    vl_logic;
        BusGrant1B      : out    vl_logic;
        BusGrant2B      : out    vl_logic;
        BusGrant3B      : out    vl_logic;
        BUSYB_B_IN      : in     vl_logic
    );
end bus_arbitration;
