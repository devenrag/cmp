library verilog;
use verilog.vl_types.all;
entity CMP_top is
    port(
        Clk             : in     vl_logic;
        Resetb          : in     vl_logic;
        fio_select_cmt0 : in     vl_logic;
        fio_select_cmt1 : in     vl_logic;
        fio_select_cmt2 : in     vl_logic;
        fio_select_cmt3 : in     vl_logic;
        fio_icache_addr_IM: in     vl_logic_vector(31 downto 0);
        fio_icache_data_in_IM: in     vl_logic_vector(127 downto 0);
        fio_icache_wea_IM: in     vl_logic;
        fio_icache_data_out_IM: out    vl_logic_vector(127 downto 0);
        fio_icache_ren_IM: in     vl_logic;
        Test_mode       : in     vl_logic;
        walking_led_en  : out    vl_logic;
        fio_reg_addr_DM : in     vl_logic_vector(31 downto 0);
        fio_reg_data_out_DM: out    vl_logic_vector(31 downto 0);
        fio_mmem_addr   : in     vl_logic_vector(31 downto 0);
        fio_mmem_data_out: out    vl_logic_vector(31 downto 0);
        fio_mmem_data_in: in     vl_logic_vector(31 downto 0);
        fio_mmem_wea    : in     vl_logic;
        fio_mmem_rea    : in     vl_logic
    );
end CMP_top;
